const URL = 'https://api.ipify.org/?format=json'

const getIP = async (url) => {
    const res = await fetch(url);
    return res.json();

}

const getLocation = async () => {
    const resIP = await getIP(URL);
    const ip = resIP.ip;

    const res = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,region,regionName,city,district,query`);
    const data = await res.json();
    return data;

}

const addInfo = async () => {
    const data = await getLocation()
    const {continent, regionName, city, district} = data

    const location = document.createElement('p')
    location.textContent = `continent: ${continent}; region name: ${regionName}; city: ${city}; district: ${district}.`;
    document.body.append(location)
}

const btn = document.querySelector('.find-location');

btn.addEventListener('click', ()=> {
    addInfo()
})

